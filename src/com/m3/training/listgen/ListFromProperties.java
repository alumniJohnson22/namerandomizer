package com.m3.training.listgen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

public class ListFromProperties implements ProcessArgs {
	
	final static Logger logger = Logger.getLogger(ListFromJson.class);

	private List<String> buildList(String filename) throws IOException {
		ArrayList<String> names = new ArrayList<>();
		Properties properties = new Properties();
		try (InputStream inStream = new FileInputStream(filename)) {
			properties.load(inStream);
		}
		List<Object> objList = properties.values().stream().collect(Collectors.toList());
		for (Object obj : objList) {
			if (obj instanceof String) {
				names.add((String) obj);
			}
		}
		return names;
	}

	public List<String> processArgs(String[] args) {
		List<String> names = new ArrayList<>();
		String msg;
		if (args.length < 1) {
			msg = "Please enter a properties file name.";
			logger.error(msg);
		}
		String filename = args[0];
		try {
			names = buildList(filename);
		} catch (FileNotFoundException e) {
			msg ="The requested file could not be found: [" + filename + "]";
			logger.error(msg);
		} catch (IOException e) {
			msg = "The requested file could not be read: [" + filename + "]";
			logger.error(msg);
		} 
		if (names.size() == 0) {
			msg = "The requested file contains no data: [" + filename + "]";
			logger.error(msg);
		}
		return names;

	}

}
